# Qu’est-ce qu’une propriété NP ?

## 1.

Un certificat est une solution, générée de manière polynomiale, au problème, cette solution peut être valide, ou non.

On peut implémenter ce certificat de manière aléatoire. En effet, on peut parcourir la liste des objets et les attribuer
aléatoirement à un sac. Dans ce cas-là, le certificat aura pour taille "n" soit une complexité O(n). Un example de
certificat :

| 2 | 1 | 3 | 4 | 2 | 4 |
|---|---|---|---|---|---|

Le premiere objet est associé au sac "2", le deuxième au sac "1", etc.

Pour vérifier notre certificat, on peut créer un tableau de n sacs, pour chaque objet on ajoute son poids dans la case
associé au sac, si le poids de la case après l"ajout d"un objet est supérieur au poids maximum c, alors on renvoie
false, si tous les objets ont été ajoutés aux sacs sans dépassement, on renvoie true.

## 2.

### 2.1

L"algorithme de la génération aléatoire peut attribuer un objet aléatoirement à un sac sans prendre aucun paramètre, les
certificats ont donc la même probabilité d"apparaître.

### 2.2

L"algorithme de vérification d"un certificat est implémenté par la classe verifiers.NaiveBinPackCertificateVerifier,
elle est de complexité O(n), étant donné que le générateur de certificat et le vérifieur sont de complexité O(n), l"
algorithme L est dit NP.

## 3. NP ⊂ EXPTIME

### 3.1

Un certificat peut prendre "k^n - 1" valeurs.

### 3.2

On peut trier les certificats dans l"ordre des valeurs des certificats.

### 3.3

On peut parcourir la liste des certificats dans l"ordre, et on s"arrête dès lors que l"on trouve une solution, si on a
parcouru tous les certificats et qu"aucune solution n"as été trouvée, on renvoie false. Cette algorithme a une
compléxité NP.

## Implémentation

### 4

Example d'utilisation du programme :
java -Dfile.encoding=UTF-8 Main "CHEMIN_VERS_BIN_PACK" [-nd|-ver|-exh] NOMBRE_SACHET

# Réductions polynomiales

