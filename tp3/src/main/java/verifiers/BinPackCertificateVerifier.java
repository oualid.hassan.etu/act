package verifiers;

import models.BinPackData;

public interface BinPackCertificateVerifier {

    /**
     * Verify that all the bags doesn't exceed the capacity of a bag
     *
     * @param binPackData             The original binpack data
     * @param objectToBagAssociations A list of index of objects associated to a bag
     * @return True if the certificat is valid, false otherwise
     */
    boolean verify(BinPackData binPackData, int bagCount, int[] objectToBagAssociations);
}
