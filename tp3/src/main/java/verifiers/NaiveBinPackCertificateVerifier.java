package verifiers;

import models.BinPackData;

public class NaiveBinPackCertificateVerifier implements BinPackCertificateVerifier {

    public boolean verify(BinPackData binPackData, int bagCount, int[] objectToBagAssociations) {
        int[] bags = new int[bagCount];

        for (int i = 0; i < objectToBagAssociations.length; i++) {
            int bagAssociatedWithObject = objectToBagAssociations[i];
            bags[bagAssociatedWithObject] += binPackData.getObjectsWeight()[i];

            if (bags[bagAssociatedWithObject] > binPackData.getBagCapacity()) {
                return false;
            }
        }

        return true;
    }
}
