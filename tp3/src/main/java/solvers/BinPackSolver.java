package solvers;

import models.BinPackData;

import java.util.Iterator;

public class BinPackSolver implements Iterator<int[]> {
    private int currentIndex;
    private final int bagCount;
    private final BinPackData binPackData;

    public BinPackSolver(BinPackData binPackData, int bagCount) {
        this.currentIndex = 0;
        this.bagCount = bagCount;
        this.binPackData = binPackData;
    }

    @Override
    public boolean hasNext() {
        return currentIndex < Math.pow(bagCount, binPackData.getObjectsWeight().length);
    }

    @Override
    public int[] next() {
        int[] certificate = new int[binPackData.getObjectsWeight().length];
        for (int x = 0; x < binPackData.getObjectsWeight().length; x++) {
            certificate[x] = (int) (currentIndex / Math.pow(bagCount, x) % bagCount);
        }
        currentIndex++;
        return certificate;
    }
}
