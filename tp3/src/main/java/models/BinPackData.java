package models;

public class BinPackData {
    private int bagCapacity;
    private int[] objectsWeight;

    public BinPackData(int bagCapacity, int[] objectsWeight) {
        this.bagCapacity = bagCapacity;
        this.objectsWeight = objectsWeight;
    }

    public int getBagCapacity() {
        return bagCapacity;
    }

    public void setBagCapacity(int bagCapacity) {
        this.bagCapacity = bagCapacity;
    }

    public int[] getObjectsWeight() {
        return objectsWeight;
    }

    public void setObjectsWeight(int[] objectsWeight) {
        this.objectsWeight = objectsWeight;
    }
}
