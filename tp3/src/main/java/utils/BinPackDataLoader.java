package utils;

import models.BinPackData;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class BinPackDataLoader {

    public static BinPackData loadFile(String path) throws FileNotFoundException {
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        int bagCapacity = scanner.nextInt();
        scanner.nextLine();
        int objectCount = scanner.nextInt();
        scanner.nextLine();
        int[] objectsWeight = new int[objectCount];
        for (int i = 0; i < objectsWeight.length; i++) {
            objectsWeight[i] = scanner.nextInt();
            scanner.nextLine();
        }

        return new BinPackData(bagCapacity, objectsWeight);
    }
}
