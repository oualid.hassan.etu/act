import generators.RandomBinPackCertificateGenerator;
import models.BinPackData;
import solvers.BinPackSolver;
import utils.BinPackDataLoader;
import verifiers.NaiveBinPackCertificateVerifier;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        String filePath = args[0];
        String mode = args[1];
        int bagCount = Integer.parseInt(args[2]);
        Scanner scanner = new Scanner(System.in);

        BinPackData binPackData = BinPackDataLoader.loadFile(filePath);

        NaiveBinPackCertificateVerifier naiveBinPackCertificateVerifier = new NaiveBinPackCertificateVerifier();
        RandomBinPackCertificateGenerator randomBinPackCertificateGenerator = new RandomBinPackCertificateGenerator();
        switch (mode) {
            case "-ver": {
                System.out.println("Please enter a certificate");
                int[] certificate = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
                boolean result = naiveBinPackCertificateVerifier.verify(binPackData, bagCount, certificate);

                System.out.println("Result: " + result);
                break;
            }
            case "-nd": {
                int[] certificate = randomBinPackCertificateGenerator.generateCertificate(binPackData, bagCount);
                System.out.println("Generated certificate: " + Arrays.toString(certificate));

                System.out.println("Result: " + naiveBinPackCertificateVerifier.verify(binPackData, bagCount, certificate));
                break;
            }
            case "-exh":
                BinPackSolver binPackSolver = new BinPackSolver(binPackData, bagCount);
                boolean isValid = false;
                int[] certificate = null;
                while (binPackSolver.hasNext() && !isValid){
                    certificate = binPackSolver.next();
                    System.out.println("Using certificate: " + Arrays.toString(certificate));
                    isValid = naiveBinPackCertificateVerifier.verify(binPackData, bagCount, certificate);
                    System.out.println("Result: " + isValid);
                }
                if (isValid) {
                    System.out.println("Valid certificate found = " + Arrays.toString(certificate));
                }
                break;
            default: {
                throw new RuntimeException("Mode not found, please specify the mode using -ver | -exh | -nd");
            }
        }
    }
}
