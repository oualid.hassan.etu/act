package generators;

import models.BinPackData;

public interface BinPackCertificateGenerator {

    /**
     * Generate a certificate for the Binpack problem
     */
    int[] generateCertificate(BinPackData binPackData, int bagCount);
}
